=begin

  Copyright 2015 Lars Bölecke

  Lizenziert unter der EUPL, nur Version 1.1 ("Lizenz");
  Sie dürfen dieses Werk ausschließlich gemäß
  dieser Lizenz nutzen.
  Eine Kopie der Lizenz finden Sie hier:


  https://joinup.ec.europa.eu/software/page/eupl

  Sofern nicht durch anwendbare Rechtsvorschriften
  gefordert oder in schriftlicher Form vereinbart, wird
  die unter der Lizenz verbreitete Software "so wie sie
  ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN -
  ausdrücklich oder stillschweigend - verbreitet.
  Die sprachspezifischen Genehmigungen und Beschränkungen
  unter der Lizenz sind dem Lizenztext zu entnehmen.



=end

class Subject


  attr_accessor :name, :type, :days, :room, :time, :teacher, :week, :last_changed

  def initialize()

    #@days= Array.new
    #@room =Array.new
    #@time =Array.new
    #@week = Array.new

  end




end