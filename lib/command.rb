=begin

  Copyright 2015 Lars Bölecke

  Lizenziert unter der EUPL, nur Version 1.1 ("Lizenz");
  Sie dürfen dieses Werk ausschließlich gemäß
  dieser Lizenz nutzen.
  Eine Kopie der Lizenz finden Sie hier:


  https://joinup.ec.europa.eu/software/page/eupl

  Sofern nicht durch anwendbare Rechtsvorschriften
  gefordert oder in schriftlicher Form vereinbart, wird
  die unter der Lizenz verbreitete Software "so wie sie
  ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN -
  ausdrücklich oder stillschweigend - verbreitet.
  Die sprachspezifischen Genehmigungen und Beschränkungen
  unter der Lizenz sind dem Lizenztext zu entnehmen.



=end
require 'optparse'

class Command



  attr_accessor :arguments






  def initialize

    @arguments = Hash.new

    optparse = OptionParser.new do|opts|

      opts.banner = "Benutzung: v2c.rb [options]"

      #Default values
      arguments[:semester] = nil
      arguments[:fsemester] = nil
      arguments[:open] = nil
      arguments[:studiengang]= nil
      arguments[:path] = "./"
      opts.on('-t sem', '--sem semester', '[WS|SS]' ) do |semester|
        if (semester.match /SS|WS/)
          arguments[:semester] = semester
        else
          puts opts
          exit
        end
      end


      opts.on('-o file', '--open file', 'HTML-Datei des VLV') do |file|
        arguments[:open] = file
      end

      opts.on( '-s sg', '--studiengang sg', 'Abkürzung des Studienganges [BT|AMW|...]') do |sg|
        arguments[:studiengang] = sg
      end

      opts.on( '-f fs', '--fachsemester fs', 'Fachsemester [1,|2|...]') do |fs|
        arguments[:fsemester] = fs
      end

      opts.on( '-p p', '--pfad p', 'Pfad zum Speichern der ics-Datein (Standard: ./)') do |p|

        if p.match /\/$/
          arguments[:path] = p
        else
          arguments[:path] = p+'\\'
        end
      end

      opts.on( '-?', '-h', '--help', 'Diese Ausgabe anzeigen' ) do
        puts opts
        exit
      end
    end


    optparse.parse!


    begin
      if arguments[:open].nil?
        raise OptionParser::MissingArgument if (arguments[:semester].nil? | arguments[:fsemester].nil? | arguments[:studiengang].nil?    )
      end
      rescue
        puts optparse
        exit
    end



  end



end