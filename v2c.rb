#!/usr/bin/env ruby

=begin

  Copyright 2015 Lars Bölecke

  Lizenziert unter der EUPL, nur Version 1.1 ("Lizenz");
  Sie dürfen dieses Werk ausschließlich gemäß
  dieser Lizenz nutzen.
  Eine Kopie der Lizenz finden Sie hier:


  https://joinup.ec.europa.eu/software/page/eupl

  Sofern nicht durch anwendbare Rechtsvorschriften
  gefordert oder in schriftlicher Form vereinbart, wird
  die unter der Lizenz verbreitete Software "so wie sie
  ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN -
  ausdrücklich oder stillschweigend - verbreitet.
  Die sprachspezifischen Genehmigungen und Beschränkungen
  unter der Lizenz sind dem Lizenztext zu entnehmen.



=end



require 'icalendar'
require 'icalendar/tzinfo'
require 'nokogiri'
require './lib/command'
require 'open-uri'
require 'win32ole'
require './lib/subject'


class V2C


  TYPES = %w[Vorlesungen Übungen Seminare]

  commandline = Command.new



  source =''


  path = commandline.arguments[:path]

  if commandline.arguments[:open].nil?

    case commandline.arguments[:semester]
      when 'SS'
        id = 'id=6'
      when 'WS'
        id = '330'
    end

    sgkurz = commandline.arguments[:studiengang]
    fs = commandline.arguments[:fsemester]

    add = "http://www.tu-ilmenau.de/vlv/index.php?id=#{id}&funccall=1&sgkurz=#{sgkurz}&vers=text&fs=#{fs}"
    html_doc = Nokogiri::HTML(open(add))
  else

    File.open(commandline.arguments[:open], 'r') do |file|
      file.readlines.each do |line|
        source += line
      end
    end
    html_doc = Nokogiri::HTML(source)

  end














  calendars = []


  tzid = "Europe/Berlin"
  tz = TZInfo::Timezone.get tzid
  timezone = tz.ical_timezone DateTime.now


  (0..5).to_a.each do |t|


    calendars[t] = Icalendar::Calendar.new
    calendars[t].prodid = 'VLV2ics'
    calendars[t].add_timezone timezone
  end


  subs =[]

  html_doc.css('div.stupla_veran div').each do |t|


    subs << Subject.new

    t.text.split("\n").each do |line|

      unless line.match /^$|^.$|Wochentag|Datum|Uhrzeit|Raum|Zielgruppe|nderungsdatum|^\s-\s$/ # skip if string is empty ^$ or string is only whitespace (\s) or ...

        if line.match /Beschreibung/
          subs[-1].name = line.gsub(/Beschreibung|(\s-\sTermin.*)/, '')
        end

        if line.match /(Lesend)/
          subs[-1].teacher = line
        end


        if line.match /bungen|Klausur|Vorlesungen|mündliche Prüfung|Praktika|Seminar|Prüf|undliche Pr/
          if subs[-1].type == nil
            subs[-1].type = line
          else
            subs[subs.length] = Subject.new
            subs[-1].name = subs[-2].name
            subs[-1].teacher = subs[-2].teacher
            subs[-1].type = line
          end
        end

        if line.match /(Montag|Dienstag|Mittwoch|Donnerstag|Freitag|Samstag|Sonntag)/


          if subs[-1].days == nil
            subs[-1].days = line
          else
            subs[subs.length] = Subject.new
            subs[-1].name = subs[-2].name
            subs[-1].teacher = subs[-2].teacher
            subs[-1].type = subs[-2].type
            subs[-1].days = line
          end

          #subs[-1].days << line
        end

        if line.match /(KW)|(\d{2}\.\d{2}\.\d{4})|(Vereinbarung im Pr)/


          if subs[-1].week == nil
            subs[-1].week = line
          else
            subs[subs.length] = Subject.new
            subs[-1].name = subs[-2].name
            subs[-1].teacher = subs[-2].teacher
            subs[-1].type = subs[-2].type
            subs[-1].days = subs[-2].days
          end


          #subs[-1].week << line
        end

        if line.match /\d{2}\.\d{2}\s-\s\d{2}\.\d{2}/

          if subs[-1].time == nil
            subs[-1].time = line
          else
            subs[subs.length] = Subject.new
            subs[-1].name = subs[-2].name
            subs[-1].teacher = subs[-2].teacher
            subs[-1].type = subs[-2].type
            subs[-1].days = subs[-2].days
            subs[-1].week = subs[-2].week
          end


          #subs[-1].time << line
        end

        if line.match /Z|Sr|RTK|Kirchhoff|Zuse|Humboldt|LdV|eigener Raum|Oe|Hs/

          if subs[-1].room == nil
            subs[-1].room = line
          else
            subs[subs.length] = Subject.new
            subs[-1].name = subs[-2].name
            subs[-1].teacher = subs[-2].teacher
            subs[-1].type = subs[-2].type
            subs[-1].days = subs[-2].days
            subs[-1].week = subs[-2].week
            subs[-1].time = subs[-2].time
          end


        end

        if line.match /Ge.ndert\sam:/
          subs[-1].last_changed = line.gsub(/Ge.ndert\sam:\s/, '')
        end


      end
    end

  end

  unless subs.empty?
    subs.each do |subject|

    unless subject.week == nil
      subject.week.split(/,/).each do |week|
        unless week.match /Vereinbarung/


          e = Icalendar::Event.new

          if subject.teacher == nil
            subject.teacher = 'Lesende(r): ???'
          end

          if (subject.room == nil)
            subject.room = '???'
          end

          e.description = subject.teacher+"\n"+"Zuletzt geändert:\s"+subject.last_changed
          e.summary = subject.name
          e.location = subject.room

          if week.match /KW|\d{2}/


            if (week.match /-/)

              kws = week.scan /\d{2}/
              starttime = subject.time.scan(/\d{2}\.\d{2}/)[0]
              endtime = subject.time.scan(/\d{2}\.\d{2}/)[1]


              case subject.days

                when 'Montag'
                  cday = 1
                when 'Dienstag'
                  cday =2
                when 'Mittwoch'
                  cday =3
                when 'Donnerstag'
                  cday =4
                when 'Freitag'
                  cday =5
                when 'Samstag'
                  cday =6
                when 'Sonntag'
                  cday =0
              end


              startdate = DateTime.commercial(DateTime.now.year, Integer(kws[0]), cday, Integer(starttime.split(/\./)[0].sub(/^0/, "")), Integer(starttime.split(/\./)[1].sub(/^0/, "")), 0, 0)
              enddate = DateTime.commercial(DateTime.now.year, Integer(kws[0]), cday, Integer(endtime.split(/\./)[0].sub(/^0/, "")), Integer(endtime.split(/\./)[1].sub(/^0/, "")), 0, 0)
              enddatefreq = DateTime.commercial(DateTime.now.year, Integer(kws[1]))


              if (enddatefreq < startdate)
                startdate = DateTime.commercial(DateTime.now.year-1, Integer(kws[0]), cday, Integer(starttime.split(/\./)[0].sub(/^0/, '')), Integer(starttime.split(/\./)[1].sub(/^0/, '')), 0, 0)
                enddate = DateTime.commercial(DateTime.now.year-1, Integer(kws[0]), cday, Integer(endtime.split(/\./)[0].sub(/^0/, '')), Integer(endtime.split(/\./)[1].sub(/^0/, '')), 0, 0)
              end


              e.dtstart = Icalendar::Values::DateTime.new startdate, 'tzid' => tzid
              e.dtend = Icalendar::Values::DateTime.new enddate, 'tzid' => tzid


              if week.match /G|U/
                e.rrule=Icalendar::Values::Recur.new('FREQ=WEEKLY,UNTIL='+enddatefreq.strftime('%Y%m%dT%H%M%zz').sub(/\+/, '')+',INTERVAL=2')
              else
                e.rrule=Icalendar::Values::Recur.new('FREQ=WEEKLY,UNTIL='+enddatefreq.strftime('%Y%m%dT%H%M%zz').sub(/\+/, ''))
              end


            end

          end

          if (week.match /(^\s\d{2}\.$)|(^\s\d{2}\.\sKW\)$)/) # only one calendar week

            w = week.scan(/\d{2}/)[0]
            starttime = subject.time.scan(/\d{2}\.\d{2}/)[0]
            endtime = subject.time.scan(/\d{2}\.\d{2}/)[1]


            case subject.days

              when 'Montag'
                cday = 1
              when "Dienstag"
                cday =2
              when "Mittwoch"
                cday =3
              when "Donnerstag"
                cday =4
              when "Freitag"
                cday =5
              when "Samstag"
                cday =6
              when "Sonntag"
                cday =0
            end


            startdate = DateTime.commercial(DateTime.now.year, Integer(w), cday, Integer(starttime.split(/\./)[0].sub(/^0/, "")), Integer(starttime.split(/\./)[1].sub(/^0/, "")), 0, 0)

            enddate = DateTime.commercial(DateTime.now.year, Integer(w), cday, Integer(endtime.split(/\./)[0].sub(/^0/, "")), Integer(endtime.split(/\./)[1].sub(/^0/, "")), 0, 0)


            e.dtstart = Icalendar::Values::DateTime.new startdate, 'tzid' => tzid
            e.dtend = Icalendar::Values::DateTime.new enddate, 'tzid' => tzid

          end


          if week.match /\d{2}\.\d{2}\.\d{4}/


            starttime = subject.time.scan(/\d{2}\.\d{2}/)[0]
            endtime = subject.time.scan(/\d{2}\.\d{2}/)[1]

            startdate = DateTime.new Integer(week.scan(/\d{4}/)[0]), Integer(week.scan(/\d{2}/)[1]), Integer(week.scan(/\d{2}/)[0]), Integer(starttime.split(/\./)[0].sub(/^0/, "")), Integer(starttime.split(/\./)[1].sub(/^0/, ""))

            enddate = DateTime.new Integer(week.scan(/\d{4}/)[0]), Integer(week.scan(/\d{2}/)[1]), Integer(week.scan(/\d{2}/)[0]), Integer(endtime.split(/\./)[0].sub(/^0/, "")), Integer(endtime.split(/\./)[1].sub(/^0/, ""))


            e.dtstart = Icalendar::Values::DateTime.new startdate, 'tzid' => tzid
            e.dtend = Icalendar::Values::DateTime.new enddate, 'tzid' => tzid

          end


          unless subject.type == nil

            case subject.type

              when /Vorlesung/
                calendars[0].add_event(e)
              when /bung/
                calendars[1].add_event(e)
              when /Seminar/
                calendars[2].add_event(e)

            end
          end
        end
      end


    end


  end


    type_counter = 0
    TYPES.each do |t|

    File.open("#{path}#{t}.ics", 'w') do |file|
      file.write(calendars[type_counter].to_ical)
    end
    type_counter = type_counter +1

  end
  else

    puts 'Es konnten keine Termine erzeugt werden, vielleicht ist die Quelle beschädigt?'

  end
  exit
end


parser = V2C.new