# V2C #

### Zusammenfassung ###

* V2C ist ein kleiner Helfer, um das [Vorlesungsverzeichnis](http://www.tu-ilmenau.de/vlv) der Technischen Universität Ilmenau in eine ics-Datei umzuwandeln.
* Version: 1.0.0
* Copyright 2015 Lars Bölecke - V2C ist lizensiert unter [EUPL v1.1](https://joinup.ec.europa.eu/software/page/eupl/licence-eupl)




### Getting Started ###


#### Installation ####

##### Für Nutzer ohne Ruby-Interpreter #####
  Nutzer, die keinen Ruby-Interpreter installiert haben und dies auch nicht tun möchten, können sich [hier](https://bitbucket.org/lb03l3ck3/v2c/downloads/v2c.exe) die passende ausführbare Datei herunterladen.

##### Für Nutzer mit Ruby #####
  Nutzer, die bereits den Ruby-Interpreter installiert haben, können sich unter [Downloads](https://bitbucket.org/lb03l3ck3/v2c/downloads) V2C als zip-Archiv herunterladen.
  Anschließend kann V2C in ein beliebiges Verzeichnis entpackt werden.

#### Benutzung ####

          Benutzung: v2c.rb [options]
                    -t, --sem semester               [WS|SS]
                    -o, --open file                  HTML-Datei des VLV
                    -s, --studiengang sg             Abkürzung des Studienganges [BT|AMW|...]
                    -f, --fachsemester fs            Fachsemester [1,|2|...]
                    -?, -h, --help                   Diese Ausgabe anzeigen




#### Abhängigkeiten ####
V2C nutzt folgende Gems:

* [Nokogiri](https://github.com/sparklemotion/nokogiri) - Lizensiert unter [MIT-License](https://github.com/sparklemotion/nokogiri)

* [icalendar](https://github.com/icalendar/icalendar) - Lizensiert unter (2-Clause BSD-License)[https://www.ruby-lang.org/en/about/license.txt]

* [tzinfo-data](https://github.com/tzinfo/tzinfo-data) - Lizensiert unter [MIT-License](https://github.com/tzinfo/tzinfo-data/blob/master/LICENSE)

* [tzinfo](https://github.com/tzinfo/tzinfo) - Lizensiert unter [MIT-License](https://github.com/tzinfo/tzinfo/blob/master/LICENSE)

* [Ocra](https://github.com/larsch/ocra) - Lizensiert unter [MIT-License](https://github.com/larsch/ocra)